"""Intro to Programming Basics - Part 1 - Challenge"""

# TODO:  Write an input statment asking the user to enter a number between 1 and 54 and assign it to a variable named 'interface'
interface = input('Enter a number between 1 and 54: ')


# TODO:  Write a print statement that displays the type of variable.  What type of variable is it?
print('The variable type is {}'.format(type(interface)))


# TODO:  Convert the data type in 'interface' to an integer and assign it to a variable named 'interface_num'
interface_num = int(interface)


# TODO:  Write a set of conditionals to:
# print "Interface Speed is 1Gb" if 'interface_num' is less than or equal to 48,
# print "Interface speed is 40Gb" if 'interface_num' is greater than 48 AND less than or equal to 54,
# print "Invalid Interface Number" if 'interface_num' is greater than 54 or less than 1.
if interface_num <= 48:
    print('Interface Speed is 1Gb')
elif interface_num > 48 and interface_num <= 54:
    print('Interface Speed is 40Gb')
else:
    print('Invalid Interface')


# TODO:  Write a function named 'square' that takes one integer as argument, squares it, and returns the result
def square(num):
    squared_num = num * num
    return squared_num


# TODO:  Call the 'square' function to calculate the following answers
print('Your number squared is', square(interface_num))  # Call the 'square' function and pass in 'interface_num' as the argument

print('21 x 21 =', square(21))

print('2342 x 2342 =', square(2342))
