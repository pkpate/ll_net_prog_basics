def cube_my_number(number):
    """
    Given a number, cube it and return the value
    """
    result = number ** 3
    return result


a = cube_my_number(5)
print("5 cubed is: ", a)
