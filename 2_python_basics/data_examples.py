# Examples of common data collections in Python

shapes = ["square", "triangle", "circle"]
books = [
    {
        "title": "War and Peace",
        "shelf": 3,
        "available": True
    },
    {
        "title": "Hamlet",
        "shelf": 1,
        "available": False
    },
    {
        "title": "Harold and the Purple Crayon",
        "shelf": 2,
        "available": True
    }
]
colors = ["blue", "green", "red", "yellow"]

print(shapes)

print(shapes[0])

print(shapes[1])

print(shapes[-1])

print(books[0]['title'])

for shape in shapes:
    print(shape)

for color in colors:
    print(color)

for color in colors:
    if color == 'blue':
        print("The color is", color)
    else:
        print("The color is not blue")
